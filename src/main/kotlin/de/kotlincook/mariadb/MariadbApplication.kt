package de.kotlincook.mariadb

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MariadbApplication : CommandLineRunner {

	@Autowired
	lateinit var myRepo: MyRepo

	override fun run(vararg args: String?) {
		val all = myRepo.findAll()
		println(all)
		val person = myRepo.findMyById(1)
		println(person)
	}
}

fun main(args: Array<String>) {
	runApplication<MariadbApplication>(*args)
}
