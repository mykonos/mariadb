package de.kotlincook.mariadb

import javax.persistence.*

@Entity
@Table(name = "mytable")
data class Person(
        @Id @GeneratedValue(strategy = GenerationType.AUTO) var id: Int,
        var name: String)