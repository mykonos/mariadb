package de.kotlincook.mariadb

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface MyRepo : CrudRepository<Person, Int> {
    override fun findAll(): List<Person>


    @Query(value = "select * from mytable where id=?1", nativeQuery = true)
    fun findMyById(id: Int): Person
}